const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
    darkMode: 'class',
    content: [
        './storage/framework/views/*.php',
        './resources/**/*.blade.php',
        './resources/**/*.ts',
    ],
    theme: {
        extend: {},
        screens: {
            xs: '480px',
            ...defaultTheme.screens,
        },
    },
    plugins: [],
}
