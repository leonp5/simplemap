const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const postCssPresetEnv = require('postcss-preset-env')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
    mode: 'production',
    module: {
        rules: [
            {
                test: /\.(sa|sc|c)ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    {
                        // apply postCSS fixes like autoprefixer and minifying
                        loader: 'postcss-loader',
                        options: {
                            postcssOptions: {
                                plugins: [
                                    require('tailwindcss'),
                                    require.resolve('autoprefixer'),
                                    require.resolve('cssnano'),
                                    // postcss-preset-env adds css prefixes
                                    // for different browsers
                                    postCssPresetEnv(),
                                ],
                            },
                        },
                    },
                    'sass-loader',
                ],
            },
        ],
    },
    plugins: [new CleanWebpackPlugin()],
}
