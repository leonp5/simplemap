const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

module.exports = (env) => {
    const envVariables = require('dotenv').config({
        path: path.resolve(__dirname, env.envFilePath),
    }).parsed

    console.log(path.resolve(__dirname, env.envFilePath))

    return {
        target: 'web',
        context: path.resolve(__dirname, '../resources/assets'),

        entry: {
            app: ['./ts/app.ts', './sass/app.scss'],
            themeCheck: ['./ts/themeCheck.ts'],
        },
        output: {
            libraryTarget: 'umd',
            libraryExport: 'default',
            path: path.resolve(__dirname, '../public'),
            filename: `${envVariables.APP_SHORTCUT}.[name].js`,
        },
        module: {
            rules: [
                {
                    // Match js, jsx, ts & tsx files
                    test: /\.[jt]sx?$/,
                    exclude: /node_modules/,
                    loader: 'esbuild-loader',
                    options: {
                        // JavaScript version to compile to
                        target: 'es2015',
                    },
                },
                {
                    test: /\.(png|jpe?g|gif)$/i,
                    use: [
                        {
                            loader: 'file-loader',
                            options: {
                                name: '[name].[ext]',
                            },
                        },
                    ],
                },
            ],
        },
        resolve: {
            extensions: ['.tsx', '.ts', '.js', '.scss'],
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: `${envVariables.APP_SHORTCUT}.[name].css`,
            }),
            new ForkTsCheckerWebpackPlugin({
                typescript: { configFile: './../../tsconfig.json' },
            }),
        ],
    }
}
