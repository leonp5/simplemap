const mix = require('laravel-mix')
const tailwindcss = require('tailwindcss')

mix.ts('resources/assets/ts/app.ts', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .options({
        postCss: [tailwindcss('./tailwind.config.js')],
    })

mix.browserSync({
    proxy: 'http://127.0.0.1:8000',

    files: [
        'src/*.php',
        'src/**/*.php',
        'routes/*.php',
        'resources/**/*.php',
        'resources/assets/ts/*.ts',
        'resources/assets/sass/*.scss',
    ],
    // => true = the browser opens a new browser window with every npm run watch startup
    open: false,
    notify: false,
    ui: false,
})
