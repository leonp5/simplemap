# Archived

Moved to [Codeberg](https://codeberg.org/leonp5/simplemap)

## Simple Map

### Build prod docker image local

1. In the root directory run: `docker build -t <custom-tag> -f .docker/online/php/Dockerfile .`

    - Optional with BuildKit enabled (makes it faster): `DOCKER_BUILDKIT=1 docker build -t <custom-tag> -f .docker/online/php/Dockerfile .`

2. Run `docker run -it -p 7000:7000 <custom-tag>` to start the image in a container local

3. Enter `http://localhost:7000` in your local browser

4. Run `docker run -it <IMAGE ID> sh` to run the image and go inside the running container with shell
