# Simple Map

## Dev setup

---

### .env file

-   The values from the `.env.dev` file are used inside the `docker-compose.yaml` and inside the webpack config (`webpack.common.js`). Keep this in mind if you change values or if you move or delete the `.env.dev` file.
    -   **Notice:** If you add a env variable in the `.env.dev` file and want it to be available inside docker you have to add it in the `docker-compose.yaml` too. For examples look in the `docker-compose.yaml` at the key `environment` in one of the services.
    -   **Notice:** If you change the project name in the `.env.dev` file you have to adopt it in the `launch.json` file too, if you use VSCode & XDebug. This counts also if you change the volume path for the project in the `docker-compose.yaml`.

### Setup docker & docker compose

-   Install docker, docker compose and [mkcert](https://github.com/FiloSottile/mkcert)

    -   Run `mkcert -install` (Creates a new local CA)

-   In `/etc/hosts` on your local machine add `127.0.1.1 simplemap.dev` (or the domain you want to use)

### Laravel

-   If you want to use default `.env` file, remove `$app->loadEnvironmentFrom('.env.dev');` from `bootstrap/app.php`.
-   With `php artisan key:generate --show` you can generate an app key and show it instead of pasting it to `.env`.

## Build prod docker image local

1. In the root directory run: `docker build -t <custom-tag> -f .docker/online/php/Dockerfile .`

    - Optional with BuildKit enabled (makes it faster): `DOCKER_BUILDKIT=1 docker build -t <custom-tag> -f .docker/online/php/Dockerfile .`

2. Run `docker run -it -p 7000:7000 <custom-tag>` to start the image in a container local

3. Enter `http://localhost:7000` in your local browser

4. Run `docker run -it <IMAGE ID> sh` to run the image and go inside the running container with shell
