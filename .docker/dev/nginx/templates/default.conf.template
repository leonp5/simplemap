upstream php-upstream {
    server php:9000;
}

# redirect asset requests to webpack devServer in dev environment
upstream assets {
    least_conn;
    server node:${WEBPACK_SERVER_PORT} max_fails=3 fail_timeout=30s;
}

# redirect webpack devServer websocket (ws) connection too
upstream ws {
    least_conn;
    server node:${WEBPACK_SERVER_PORT} max_fails=3 fail_timeout=30s;
}

upstream php-xdebug-upstream {
    server php-xdebug:${XDEBUG_PORT};
}

map $cookie_XDEBUG_SESSION $fastcgi_pass {
    default php-upstream;
    ${XDEBUG_IDE_KEY} php-xdebug-upstream;
}

server {
    listen 80;
    server_name ${APP_DOMAIN};

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }
}

server {
    listen 443 ssl;
    server_name ${APP_DOMAIN};

    root /var/www/${APP_SHORTCUT}/public;

    index index.php index.html;

    # switch logging to console out to view in docker
    access_log /dev/stdout;
    error_log /dev/stderr;
    rewrite_log on;

    # redirect to index.php when php file not found
    location / {
        try_files $uri $uri/ /index.php?$query_string;
        autoindex on;
    }

    # php fpm
    location ~ \.php$ {
        try_files $uri /index.php =404;
        fastcgi_pass $fastcgi_pass;
        fastcgi_index index.php;
        fastcgi_buffers 16 16k;
        fastcgi_buffer_size 32k;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_read_timeout 600;
        include fastcgi_params;
        autoindex on;
    }

    # redirect asset requests to webpack devServer in dev environment
    location ~ /${APP_SHORTCUT}/assets/(?<url>.*) {
            proxy_pass http://assets/$url;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection 'upgrade';
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $remote_addr;
            proxy_cache_bypass $http_upgrade;
    }

    # redirect webpack devServer websocket(ws) connection too
    location ~ /ws {
        proxy_pass http://ws;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_cache_bypass $http_upgrade;
    }

    ssl_certificate /etc/nginx/certs/self-signed/simplemap.dev+5.pem;
    ssl_certificate_key /etc/nginx/certs/self-signed/simplemap.dev+5-key.pem;

    # nginx cache control
    location ~* \.(?:css(\.map)?|js(\.map)?|jpe?g|png|gif|ico|cur|heic|webp|tiff?|mp3|m4a|aac|ogg|midi?|wav|mp4|mov|webm|mpe?g|avi|ogv|flv|wmv)$ {
        expires 2d;
        add_header Cache-Control "public,max-age=31536000";
        access_log off;
    }

    # svg, fonts
    location ~* \.(?:svgz?|ttf|ttc|otf|eot|woff2?)$ {
        expires 2d;
        add_header Access-Control-Allow-Origin "*";
        add_header Cache-Control "public,max-age=31536000";
        access_log off;
    }

    # gzip
    gzip on;
    gzip_vary on;
    gzip_proxied any;
    gzip_comp_level 6;
    gzip_types text/plain text/css text/xml application/json application/javascript application/rss+xml application/atom+xml image/svg+xml;
}
