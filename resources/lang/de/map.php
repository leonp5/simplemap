<?php

return [
    'search' => 'Suchen',
    'no-result' => 'Keine Ergebnisse',
    'alt-results' => 'Alternative Suchergebnisse'
];
