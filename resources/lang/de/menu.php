<?php

return [
    'disclaimer' => 'Impressum',
    'theme.dark' => 'Dunkel',
    'theme.light' => 'Hell',
    'theme.system' => 'System',
];
