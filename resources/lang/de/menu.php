<?php

return [
    'disclaimer' => 'Impressum',
    'map' => 'Zur Karte',

    'theme.dark' => 'Dunkel',
    'theme.light' => 'Hell',
    'theme.system' => 'System',
    'map.satellite' => 'Satellit',
    'map.standard' => 'Standard',
];
