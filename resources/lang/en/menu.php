<?php

return [
    'disclaimer' => 'Disclaimer',
    'theme.dark' => 'Dark',
    'theme.light' => 'Light',
    'theme.system' => 'System',
];
