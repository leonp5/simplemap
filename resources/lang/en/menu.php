<?php

return [
    'disclaimer' => 'Disclaimer',
    'map' => 'Map',

    'theme.dark' => 'Dark',
    'theme.light' => 'Light',
    'theme.system' => 'System',
    'map.satellite' => 'Satelite',
    'map.standard' => 'Standard',
];
