<?php

return [
    'search'   => 'Search',
    'no-result' => 'No results',
    'alt-results' => 'Alternative search results'
];
