@extends('layout.app')

@section('search')
    @include('map.partials.search')
@endsection

@section('content')
@include('map.partials.alternative-results-open')
@include('map.partials.alternative-results')
<div class="flex-1 relative" data-{{ Config::get('app.name_short') }}-function="Map">
    <input type="hidden" value="{{ $searchValue ?? ''}}" id="{{ Config::get('app.name_short') }}-initial-search" />
    @include('map.partials.zoom-buttons')

    <div class="absolute z-[1002] top-24 right-2 bg-stone-50 dark:bg-stone-900 text-stone-900 hover:text-stone-500 dark:text-emerald-200 dark:hover:text-emerald-300 drop-shadow-md">
        <div id="{{ Config::get('app.name_short') }}-locate" class="cursor-pointer p-3">
            <i class="gg-track"></i>
        </div>
    </div>

    {{-- <div class="absolute z-[1002] bottom-20 right-2 bg-stone-50 dark:bg-stone-900 text-stone-900 hover:text-stone-500 dark:text-emerald-200 dark:hover:text-emerald-300 drop-shadow-md">
        <div id="{{ Config::get('app.name_short') }}-routing" class="cursor-pointer p-3">
            <x-svg svg="direction" viewBox="24 24" width="24" height="24" class="text-stone-900 hover:text-stone-500 dark:text-emerald-200 dark:hover:text-emerald-300"/>
        </div>
    </div> --}}

</div>
@endsection
