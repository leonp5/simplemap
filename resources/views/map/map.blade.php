@extends('layout.app')

@section('search')
    @include('map.partials.search')
@endsection

@section('content')
@include('map.partials.alternative-results-open')
@include('map.partials.alternative-results')
<div class="flex-1" data-{{ Config::get('app.name_short') }}-function="Map">
</div>
@endsection