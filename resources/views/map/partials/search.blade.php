<div class="px-4 grow">
    <form class="w-full inline-block relative">
        <input
        class="shadow appearance-none w-full md:w-5/6 xl:w-3/6 py-2 px-1 text-gray-700 dark:bg-stone-600 dark:text-stone-300 leading-tight focus:outline-none focus:shadow-outline"
        type="search" placeholder="{{__('map.search')}}..." id="{{ Config::get('app.name_short') }}-map-search">
        <div class="hidden z-[1003] ml-0 mr-0 absolute w-11/12 md:w-5/6 xl:w-3/6 rounded-b shadow-md bg-white dark:bg-stone-600 ring-0 ring-opacity-5 divide-y divide-gray-100 focus:outline-none"
        role="menu" aria-orientation="vertical" aria-labelledby="menu-button" tabindex="-1"
        id="{{ Config::get('app.name_short') }}-result-list">
        <div class="hidden py-1" role="none" id="{{ Config::get('app.name_short') }}-no-result">
            <span class="text-gray-400 dark:text-stone-400 block px-2 py-1 text-sm" role="menuitem"
            tabindex="-1">{{__('map.no-result')}}</span>
        </div>
        <div class="hidden py-1" role="none" id="{{ Config::get('app.name_short') }}-search-result-item-template">
            <a href="" class="text-gray-700 dark:text-stone-300 block px-2 py-1 text-sm hover:bg-gray-200 dark:hover:bg-stone-900" role="menuitem"
            tabindex="-1"></a>
        </div>
    </div>
</form>
</div>