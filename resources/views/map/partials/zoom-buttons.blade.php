<div class="absolute z-[1002] top-2 right-2 bg-stone-50 dark:bg-stone-900 text-stone-900 dark:text-emerald-200 drop-shadow-md">
    <div id="{{ Config::get('app.name_short') }}-zoom-in" class="cursor-pointer py-4 px-2 hover:text-stone-500 dark:hover:text-emerald-300">
        <i class="gg-math-plus"></i>
    </div>
    <div class="border-b-2 border-stone-800 dark:border-emerald-300"></div>
    <div id="{{ Config::get('app.name_short') }}-zoom-out" class="py-4 px-2 cursor-pointer hover:text-stone-500 dark:hover:text-emerald-300">
        <i class="gg-math-minus"></i>
    </div>
</div>