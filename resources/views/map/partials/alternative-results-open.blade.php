<div class="absolute z-[1002] left-0 pt-3 pr-1 bg-blue-100 dark:bg-stone-900 h-[40px] w-[32px] hidden">
    <div class="relative">
        <div class="{{ Config::get('app.name_short') }}-arrow-wrapper arrow-right right-0 absolute"
            id="{{ Config::get('app.name_short') }}-alternative-results-open-button">
            <div class="{{ Config::get('app.name_short') }}-arrow-box">
                <div class="{{ Config::get('app.name_short') }}-arrow-inner bg-gray-500 dark:bg-emerald-400"></div>
            </div>
        </div>
    </div>
</div>