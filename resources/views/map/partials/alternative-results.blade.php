<div class="{{ Config::get('app.name_short') }}-alternative-results bg-blue-100 dark:bg-stone-900">
    <div class="relative mb-3 mr-5 mt-3">
        <div class="pl-8 text-stone-900 dark:text-stone-400">{{__('map.alt-results')}}</div>
        <div class="{{ Config::get('app.name_short') }}-arrow-wrapper arrow-right right-0 top-0 absolute"
            id="{{ Config::get('app.name_short') }}-alternative-results-close-button">
            <div class="{{ Config::get('app.name_short') }}-arrow-box">
                <div class="{{ Config::get('app.name_short') }}-arrow-inner bg-gray-500 dark:bg-emerald-400"></div>
            </div>
        </div>
    </div>
    <div class="p-8 shadow-md overflow-auto">
        <a href="" id="{{ Config::get('app.name_short') }}-alternative-result-item-template"
            class="hidden text-sm text-stone-900 hover:text-stone-500 dark:text-stone-300 dark:hover:text-emerald-300"></a>
        <div class="flex flex-col space-y-6" id="{{ Config::get('app.name_short') }}-alternative-result-item-list">
           
        </div>
    </div>
</div>