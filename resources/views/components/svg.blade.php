<svg
    width="{{ $width }}"
    height="{{ $height }}"
    viewBox="0 0 {{ $viewBox }}"
    fill="{{ $fill }}"
    stroke="{{ $stroke }}"
    stroke-width="{{ $strokeWidth }}"
    stroke-linecap="round"
    stroke-linejoin="round"
    id="{{ $id }}"
    {{ $attributes->merge(['class' => $class])}}
    >
    @includeIf("svg.$svg")
</svg>