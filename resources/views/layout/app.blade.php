<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="32x32" href="/sm32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/sm16.png">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @if(View::hasSection('title'))
        @yield('title')
        @else
        SimpleMap
        @endif
    </title>

    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">

    <script>
        // It's best to inline this in `head` to avoid FOUC (flash of unstyled content) when changing pages or themes
        if (
          localStorage.getItem('sm-theme') === 'dark' ||
          (!('color-theme' in localStorage) &&
            window.matchMedia('(prefers-color-scheme: dark)').matches)
        ) {
          document.documentElement.classList.add('dark');
        } else {
          document.documentElement.classList.remove('dark');
        }
      </script>

    @yield('styles')
</head>

<body>
    <div class="flex flex-col min-h-screen">
        @include('layout.partials.header')
        <main class="flex-1 flex overflow-y-auto">
            @yield('content')
        </main>
    </div>
    @yield('scripts')
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>