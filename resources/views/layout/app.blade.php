<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" type="image/png" sizes="32x32" href="/sm32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/sm16.png">

    <link rel="search" type="application/opensearchdescription+xml" href="{{route('xmlSearchFileDownload')}}" title="SimpleMap">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>
        @if(View::hasSection('title'))
        @yield('title')
        @else
        SimpleMap
        @endif
    </title>

    <link href="{{ asset('sm.app.css') }}" rel="stylesheet" type="text/css">

    <script src="{{ asset('sm.themeCheck.js') }}">
      // It's best to inline this in `head` to avoid FOUC (flash of unstyled content) when changing pages or themes
      </script>

    @yield('styles')
</head>

<body>
    <div class="flex flex-col min-h-screen">
        @include('layout.partials.header')
        <main class="flex-1 flex overflow-y-auto">
            @yield('content')
        </main>
    </div>
    @yield('scripts')
    <script src="{{ asset('sm.app.js') }}"></script>
</body>

</html>
