<header class="flex justify-between items-center px-4 py-4 bg-blue-100 dark:bg-stone-900">
  <div class="flex items-center w-full">
    <a class="mx-3" href="/">
      <i class="gg-globe dark:text-emerald-400" style="--ggs: 1.7;"></i>
    </a>
      <h3 class="hidden ml-3 text-2xl font-medium text-blue-500 dark:text-emerald-100 sm:block">Simple Map</h3>
    @yield('search')
  </div>
  @include('layout.partials.menu')
</header>