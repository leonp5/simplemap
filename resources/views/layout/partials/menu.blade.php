<nav data-{{ Config::get('app.name_short') }}-function="HamburgerMenu">
    <button class="text-gray-500 dark:text-emerald-400 w-10 h-10 relative focus:outline-none">
        <div class="block w-5 absolute left-1/2 top-1/2 transform -translate-x-1/2 -translate-y-1/2">
            <span aria-hidden="true" class="block absolute h-0.5 w-5 bg-current transform transition duration-500 ease-in-out -translate-y-1.5"></span>
            <span aria-hidden="true" class="block absolute h-0.5 w-5 bg-current transform transition duration-500 ease-in-out"></span>
            <span aria-hidden="true" class="block absolute h-0.5 w-5 bg-current transform transition duration-500 ease-in-out translate-y-1.5"></span>
        </div>
    </button>
    <div class="{{ Config::get('app.name_short') }}-hamburger-menu top-16 h-5/6 w-full xs:w-48">
        <div class="flex flex-col h-full p-10 bg-blue-100 dark:bg-stone-900 shadow-md overflow-auto">
            <div class="flex flex-col space-y-6 flex-none mb-5">
                <a href="#" class="text-sm text-stone-900 hover:text-stone-500 dark:text-emerald-200 dark:hover:text-emerald-300">Menu2</a>
                <a href="#" class="text-sm text-stone-900 hover:text-stone-500 dark:text-emerald-200 dark:hover:text-emerald-300">Menu3</a>
                <a href="#" class="text-sm text-stone-900 hover:text-stone-500 dark:text-emerald-200 dark:hover:text-emerald-300">Menu3</a>
                <a href="{{route('disclaimer')}}" class="text-sm text-stone-900 hover:text-stone-500 dark:text-emerald-200 dark:hover:text-emerald-300">{{__('menu.disclaimer')}}</a>
            </div>
            
            <div class="grow"></div>

            <div class="flex flex-col items-center w-full my-2" data-{{ Config::get('app.name_short') }}-function="ThemeSwitch">
                <div class="flex flex-col items-center my-4 hidden" style="--ggs: 0.7;" id="{{Config::get('app.name_short')}}-theme-settings">
                    <div class="flex items-center my-1 cursor-pointer" id="{{Config::get('app.name_short')}}-theme-system">
                        <i class="gg-screen dark:text-emerald-400"></i>
                        <p class="text-sm text-stone-900 hover:text-stone-500 dark:text-emerald-100 ml-1">{{__('menu.theme.system')}}</p>
                    </div>
                    <div class="flex items-center my-1 cursor-pointer" id="{{Config::get('app.name_short')}}-theme-light">
                        <i class="gg-sun dark:text-emerald-400"></i>
                        <p class="text-sm text-stone-900 hover:text-stone-500 dark:text-emerald-100 ml-1">{{__('menu.theme.light')}}</p>
                    </div>
                    <div class="flex items-center my-1 cursor-pointer" id="{{Config::get('app.name_short')}}-theme-dark">
                        <i class="gg-moon dark:text-emerald-400"></i>
                        <p class="text-sm text-stone-900 hover:text-stone-500 dark:text-emerald-100 ml-1">{{__('menu.theme.dark')}}</p>
                    </div>
                </div>
                <i class="" id="{{Config::get('app.name_short')}}-current-theme"></i>
            </div>

            <div class="flex justify-center w-full mt-2">
                <a class="text-sm text-gray-800 dark:text-emerald-200 dark:hover:text-emerald-400" href="{{ route('localization', ['locale' => 'gb']) }}">EN</a>
                <p class="text-sm text-gray-800 mx-1 dark:text-emerald-200">|</p>
                <a class="text-sm text-gray-800 dark:text-emerald-200 dark:hover:text-emerald-400" href="{{ route('localization', ['locale' => 'de']) }}">DE</a>
            </div>
        </div>
    </div>
    
</nav>