@extends('layout.app')

@section('title')
{{__('disclaimer.title')}} | {{Config::get('app.name')}}
@endsection

@section('content')
<div class="flex-1 bg-white dark:bg-stone-700 justify-center text-center text-slate-800 dark:text-stone-300">
    <p class="text-2xl my-5 text-inherit">{{__('disclaimer.technique')}}</p>
    <p class="text-inherit my-5">Leon Pelzer</p>
    <p class="font-bold text-inherit my-5">E-Mail: <a
            class="font-medium underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
            href="mailto:leonpe@posteo.de">leonpe@posteo.de</a></p>
    <p class="font-bold text-inherit my-5">GitLab: <a target="_blank"
            class="font-medium underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
            href="https://gitlab.com/leonp5">{{__('disclaimer.gitlab')}}</a></p>
    <p class="font-bold text-inherit my-5">Codeberg: <a target="_blank"
        class="font-medium underline text-blue-600 hover:text-blue-800 visited:text-purple-600"
        href="https://codeberg.org/leonp5">{{__('disclaimer.gitlab')}}</a></p>
    <p class="text-2xl mt-10 mb-5 text-inherit">{{__('disclaimer.technique-used')}}</p>
    <div class="flex justify-center text-center text-inherit">
        <a class="font-medium underline text-blue-600 hover:text-blue-800 visited:text-purple-600" target="_blank"
            href="https://www.php.net/">PHP:</a>
        <p class="ml-2 font-bold text-inherit">{{ PHP_VERSION }}</p>
    </div>
    <div class="flex justify-center text-center text-inherit">
        <a class="font-medium underline text-blue-600 hover:text-blue-800 visited:text-purple-600" target="_blank"
            href="https://www.laravel.com/">Laravel:</a>
        <p class="ml-2 font-bold text-inherit">{{ Illuminate\Foundation\Application::VERSION }}</p>
    </div>
    <div class="flex justify-center text-center text-inherit">
        <a class="font-medium underline text-blue-600 hover:text-blue-800 visited:text-purple-600" target="_blank"
            href="https://www.leafletjs.com/">Leaflet:</a>
        <p class="ml-2 font-bold text-inherit">1.9.3</p>
    </div>
    <div class="flex justify-center text-center text-inherit">
        <a class="font-medium underline text-blue-600 hover:text-blue-800 visited:text-purple-600" target="_blank"
            href="https://www.typescriptlang.org/">Typescript:</a>
        <p class="ml-2 font-bold text-inherit">5.0.3</p>
    </div>
    <div class="flex justify-center text-center text-inherit">
        <a class="font-medium underline text-blue-600 hover:text-blue-800 visited:text-purple-600" target="_blank"
            href="https://tailwindcss.com">Tailwind:</a>
        <p class="ml-2 font-bold text-inherit">3.3.1</p>
    </div>
</div>
@endsection
