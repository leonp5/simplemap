export default interface InitialClassInterface {
    init(htmlElement: Element): void
}
