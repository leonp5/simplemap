import MapSearch from './search/MapSearch'
import SearchResultInterface from './search/SearchResultInterface'

export default class AlternativeResults {
    alternativeResultsCloseButton: HTMLElement
    alternativeResultsOpenButton: HTMLElement
    alternativeResultsElement: Element
    alternativeResultsList: HTMLElement
    alternativeResultItemTemplate: HTMLElement

    constructor() {
        this.alternativeResultsCloseButton = document.getElementById(
            `${globalThis.appShortCut}-alternative-results-close-button`
        )!
        this.alternativeResultsOpenButton = document.getElementById(
            `${globalThis.appShortCut}-alternative-results-open-button`
        )!
        this.alternativeResultsElement = document.getElementsByClassName(
            `${globalThis.appShortCut}-alternative-results`
        )[0]
        this.alternativeResultsList = document.getElementById(
            `${globalThis.appShortCut}-alternative-result-item-list`
        )!
        this.alternativeResultItemTemplate = document.getElementById(
            `${globalThis.appShortCut}-alternative-result-item-template`
        )!

        this.init()
    }

    init() {
        this.alternativeResultsCloseButton.addEventListener('click', () => {
            this.toggleAlternativeResults()
        })

        this.alternativeResultsOpenButton.addEventListener('click', () => {
            this.toggleAlternativeResults()
        })
    }

    toggleAlternativeResults() {
        this.alternativeResultsCloseButton.classList.toggle('arrow-left')
        this.alternativeResultsCloseButton.classList.toggle('arrow-right')
        this.alternativeResultsOpenButton.classList.toggle('arrow-left')
        this.alternativeResultsOpenButton.classList.toggle('arrow-right')
        this.alternativeResultsElement.classList.toggle(
            `${globalThis.appShortCut}-alternative-results-open`
        )
    }

    showOpenButton() {
        if (
            this.alternativeResultsOpenButton.parentNode &&
            this.alternativeResultsOpenButton.parentNode.parentNode
        ) {
            const parentElement = this.alternativeResultsOpenButton.parentNode
                .parentNode as HTMLElement

            if (parentElement.classList.contains('hidden')) {
                parentElement.classList.remove('hidden')
            }
        } else {
            console.error('Something wrong with the open button')
        }
    }

    createAlternativeResultsElements(
        selectedSearchResult: SearchResultInterface,
        allSearchResults: Array<SearchResultInterface>,
        mapSearch: MapSearch
    ) {
        while (this.alternativeResultsList.firstChild) {
            if (this.alternativeResultsList.lastChild) {
                this.alternativeResultsList.removeChild(
                    this.alternativeResultsList.lastChild
                )
            }
        }

        allSearchResults.forEach((searchResultData: any) => {
            const alternativeResultItem =
                this.alternativeResultItemTemplate.cloneNode(
                    true
                ) as HTMLElement
            alternativeResultItem.innerHTML = searchResultData.display_name

            if (
                searchResultData.display_name ===
                selectedSearchResult.display_name
            ) {
                alternativeResultItem.classList.remove('text-stone-900')
                alternativeResultItem.classList.remove('dark:text-stone-300')
                alternativeResultItem.classList.add('text-stone-500')
                alternativeResultItem.classList.add('dark:text-emerald-400')
            }

            this.alternativeResultsList.append(alternativeResultItem)
            alternativeResultItem.addEventListener('click', (e) => {
                e.preventDefault()
                mapSearch.goToResult(searchResultData)
                this.toggleAlternativeResults()
            })
            alternativeResultItem.classList.remove('hidden')
        })
    }
}
