export default class Routing {
    map: L.Map
    openRoutingButton: HTMLElement
    closeRoutingButton: HTMLElement

    constructor(map: L.Map) {
        this.map = map
    }

    init(): void {
        console.log('Routing runs')
    }
}
