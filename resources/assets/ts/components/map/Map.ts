import L, { LatLngExpression } from 'leaflet'
import 'leaflet/dist/leaflet.css'
import InitialClassInterface from '../../global/interfaces/InitialClassInterface'
import MapMenu from './MapMenu'
import LocateUser from './LocateUser'
import Routing from './Routing'
import MapSearch from './search/MapSearch'
import MapThemeInterface from './interfaces/MapThemeInterface'
/* This code is needed to properly load the images in the Leaflet CSS */
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
delete L.Icon.Default.prototype._getIconUrl
L.Icon.Default.mergeOptions({
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png').default,
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    iconUrl: require('leaflet/dist/images/marker-icon.png').default,
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    shadowUrl: require('leaflet/dist/images/marker-shadow.png').default,
    // shadowUrl: iconShadow,
})

export default class Map implements InitialClassInterface {
    map: L.Map
    searchExpression?: string
    selectedTheme: MapThemeInterface
    darkTheme: MapThemeInterface
    lightTheme: MapThemeInterface
    arcgisSateliteTheme: MapThemeInterface
    zoomInButton: HTMLElement
    zoomOutButton: HTMLElement
    satelliteStyleButton: HTMLElement
    standardStyleButton: HTMLElement
    activeMapLayer: L.TileLayer
    internationalMapLayer: string
    germanMapLayer: string
    langIso2: string
    initialSeachValue?: string

    constructor() {
        this.langIso2 = document.documentElement.lang
        this.internationalMapLayer = 'https://{s}.tile.osm.org/{z}/{x}/{y}.png'
        this.germanMapLayer =
            'https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png'

        this.arcgisSateliteTheme = {
            urlTemplate:
                'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
            options: {
                maxZoom: 21,
                maxNativeZoom: 24,
                attribution:
                    'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community',
            },
        }

        this.lightTheme = {
            urlTemplate:
                this.langIso2 === 'de'
                    ? this.germanMapLayer
                    : this.internationalMapLayer,
            options: {
                maxZoom: 20,
                maxNativeZoom: 19,
                attribution:
                    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            },
        }

        const initialSearchInputElement = document.getElementById(
            `${globalThis.appShortCut}-initial-search`
        ) as HTMLInputElement

        this.initialSeachValue = initialSearchInputElement.value
    }

    init(htmlElement: HTMLElement): void {
        this.selectedTheme = this.lightTheme
        this.zoomInButton = document.getElementById(
            `${globalThis.appShortCut}-zoom-in`
        )
        this.zoomOutButton = document.getElementById(
            `${globalThis.appShortCut}-zoom-out`
        )

        document
            .getElementById(`${globalThis.appShortCut}-satellite-style`)
            .addEventListener('click', () => {
                this.setMapLayerSatellite()
            })

        document
            .getElementById(`${globalThis.appShortCut}-standard-style`)
            .addEventListener('click', () => {
                this.setMapLayerStandard()
            })

        const coordinates = [51.152, 9.47] as LatLngExpression
        this.initMap(htmlElement, coordinates)
    }

    initMap(htmlElement: HTMLElement, coordinates: LatLngExpression): void {
        const isPc = this.checkDeviceType()
        const enableScrollWheelZoom = isPc

        const options: L.MapOptions = {
            scrollWheelZoom: enableScrollWheelZoom,
            doubleClickZoom: true,
            dragging: true,
            zoomControl: false,
        }

        this.map = L.map(htmlElement, options).setView(coordinates, 6)

        this.setMapLayerStandard()

        L.control.scale().addTo(this.map)

        this.zoomInButton.addEventListener('click', () => {
            this.map.zoomIn()
        })

        this.zoomOutButton.addEventListener('click', () => {
            this.map.zoomOut()
        })

        globalThis.map = this.map

        this.initLocation()
        this.initMapMenu()
        this.initSearch()
    }

    checkDeviceType(): boolean {
        if (
            /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
                navigator.userAgent
            )
        ) {
            return false
        } else {
            return true
        }
    }

    setMapLayerSatellite(): void {
        this.selectedTheme = this.arcgisSateliteTheme

        this.setMapLayer()
    }

    setMapLayerStandard(): void {
        this.selectedTheme = this.lightTheme

        this.setMapLayer()
    }

    setMapLayer(): void {
        if (this.activeMapLayer !== undefined) {
            this.map.removeLayer(this.activeMapLayer)
        }

        this.activeMapLayer = L.tileLayer(
            this.selectedTheme.urlTemplate,
            this.selectedTheme.options
        )

        this.activeMapLayer.addTo(this.map)
    }

    initLocation(): void {
        new LocateUser(this.map).init()
    }

    initSearch(): void {
        new MapSearch(this.map).init(this.initialSeachValue)
    }

    initMapMenu(): void {
        new MapMenu(this.map).init()
    }

    initRouting(): void {
        new Routing(this.map).init()
    }
}
