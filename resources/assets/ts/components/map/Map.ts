import L, { LatLngTuple } from 'leaflet'
import 'leaflet/dist/leaflet.css'
import InitialClassInterface from '../../global/interfaces/InitialClassInterface'
import MapMenu from './MapMenu'
import MapSearch from './search/MapSearch'
import MapThemeInterface from './MapThemeInterface'
/* This code is needed to properly load the images in the Leaflet CSS */
// @ts-ignore
delete L.Icon.Default.prototype._getIconUrl
L.Icon.Default.mergeOptions({
    iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png').default,
    iconUrl: require('leaflet/dist/images/marker-icon.png').default,
    shadowUrl: require('leaflet/dist/images/marker-shadow.png').default,
})

export default class Map implements InitialClassInterface {
    map: L.Map
    searchExpression?: string
    urlTemplate: string
    mapOptions: L.TileLayerOptions
    darkTheme: MapThemeInterface
    lightTheme: MapThemeInterface

    constructor() {
        this.lightTheme = {
            urlTemplate:
                'https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png',
            options: {
                attribution:
                    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
            },
        }

        this.darkTheme = {
            urlTemplate:
                'https://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}{r}.png',
            options: {
                attribution:
                    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>',
            },
        }
    }

    init(htmlElement: HTMLElement): void {
        if (document.documentElement.classList.contains('dark') === true) {
            this.urlTemplate = this.darkTheme.urlTemplate
            this.mapOptions = this.darkTheme.options
        } else {
            this.urlTemplate = this.lightTheme.urlTemplate
            this.mapOptions = this.lightTheme.options
        }

        const coordinates = [51.152, 9.47] as LatLngTuple
        this.initMap(htmlElement, coordinates)
    }

    initMap(htmlElement: HTMLElement, coordinates: LatLngTuple) {
        const isPc = this.checkDeviceType()
        const enableScrollWheelZoom = isPc

        this.map = L.map(htmlElement, {
            scrollWheelZoom: enableScrollWheelZoom,
            doubleClickZoom: true,
            dragging: true,
        }).setView(coordinates, 6)

        this.map.zoomControl.setPosition('topright')

        L.tileLayer(this.urlTemplate, this.mapOptions).addTo(this.map)

        L.control.scale().addTo(this.map)

        globalThis.map = this.map

        this.initMapMenu()
        this.initSearch()
    }

    checkDeviceType() {
        if (
            /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
                navigator.userAgent
            )
        ) {
            return false
        } else {
            return true
        }
    }

    initSearch() {
        new MapSearch(this.map)
    }

    initMapMenu() {
        new MapMenu(this.map)
    }
}
