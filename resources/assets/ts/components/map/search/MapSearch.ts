import L, { LatLngExpression } from 'leaflet'
import Fetch from '../../fetch/Fetch'
import AlternativeResults from './AlternativeResults'
import SearchResultInterface from './SearchResultInterface'

export default class MapSearch {
    map: L.Map
    baseTextSearchUrl: string
    fetch: Fetch
    alternativeResults: AlternativeResults
    searchInput: HTMLInputElement
    itemList: HTMLElement
    resultItemElementTemplate: HTMLElement
    typeInterval: number
    typingTimer: NodeJS.Timeout | null
    noResultItem: HTMLElement
    visibleMarker: L.Marker<unknown>
    countdownPassed: boolean
    allSearchResults: Array<SearchResultInterface>

    constructor(map: L.Map) {
        this.map = map
        this.baseTextSearchUrl =
            'https://nominatim.openstreetmap.org/search?format=jsonv2&q='
        this.fetch = new Fetch()
        this.alternativeResults = new AlternativeResults()
        this.searchInput = document.getElementById(
            `${globalThis.appShortCut}-map-search`
        ) as HTMLInputElement
        this.visibleMarker = null
        this.countdownPassed = false
        this.typingTimer = null
        this.typeInterval = 300
        this.itemList = document.getElementById(
            `${globalThis.appShortCut}-result-list`
        )
        this.resultItemElementTemplate = document.getElementById(
            `${globalThis.appShortCut}-search-result-item-template`
        )
        this.noResultItem = document.getElementById(
            `${globalThis.appShortCut}-no-result`
        )
        this.allSearchResults = null
    }

    init(initialSeachValue = '') {
        try {
            if (initialSeachValue.length > 0) {
                this.#doInitialSearch(initialSeachValue)
            }

            this.searchInput.addEventListener('keyup', (e: KeyboardEvent) => {
                console.log(e)
                e.preventDefault()

                if (this.typingTimer) {
                    clearTimeout(this.typingTimer)
                }

                if (
                    e.code === 'ArrowRight' ||
                    e.code === 'ArrowLeft' ||
                    e.code === 'Escape'
                ) {
                    return
                }

                if (e.code === 'ArrowUp' || e.code === 'ArrowDown') {
                    if (!this.itemList.classList.contains('hidden')) {
                        // TODO add navigation via arrow down & up
                    }
                    return
                }

                if (!this.itemList.classList.contains('hidden')) {
                    this.#hideItemList()
                }

                if (this.searchInput.value.length > 0) {
                    const url = this.baseTextSearchUrl + this.searchInput.value
                    this.typingTimer = setTimeout(() => {
                        this.#doApiFetch(url)
                    }, this.typeInterval)
                }
            })
        } catch (e) {
            console.error(e)
        }
    }

    async #doInitialSearch(initialSeachValue: string) {
        const url = this.baseTextSearchUrl + initialSeachValue
        const fetchOptions = this.fetch.createFetchOptionsForApi('GET', url)
        const response = await this.fetch.doFetch(fetchOptions)

        if (response.status === 200) {
            const responseData = await response.json()
            if (responseData.length > 0) {
                this.allSearchResults = responseData
                this.goToResult(responseData[0])
                return
            }

            if (responseData.length === 0) {
                this.#showNoResultHint()
                this.#showItemList()
            }
        }
    }

    async #doApiFetch(url: string) {
        const fetchOptions = this.fetch.createFetchOptionsForApi('GET', url)
        const response = await this.fetch.doFetch(fetchOptions)
        this.#handleResponse(response)
    }

    async #handleResponse(response: Response) {
        if (response.status === 200) {
            const responseData = await response.json()
            this.#removeOldResults()
            if (responseData.length > 0) {
                this.#showResults(responseData)
                return
            }

            if (responseData.length === 0) {
                this.#showNoResultHint()
                this.#showItemList()
            }
        }
    }

    async #showResults(responseData: Promise<Array<SearchResultInterface>>) {
        this.#hideNoResultHint()
        this.allSearchResults = await responseData
        let count = 0
        this.allSearchResults.forEach(
            (searchResultData: SearchResultInterface) => {
                count++
                const resultItem = this.resultItemElementTemplate.cloneNode(
                    true
                ) as HTMLElement

                const anchorElement = resultItem.querySelector('a')

                if (anchorElement) {
                    anchorElement.innerHTML = searchResultData.display_name
                    resultItem.setAttribute(
                        'id',
                        `${globalThis.appShortCut}-result-item-${count}`
                    )
                    this.itemList.append(resultItem)
                    resultItem.addEventListener('click', (e) => {
                        e.preventDefault()
                        this.goToResult(searchResultData)
                    })
                    resultItem.classList.remove('hidden')
                }
            }
        )
        this.#showItemList()
    }

    #addMarker(selectedSearchResult: SearchResultInterface) {
        if (this.visibleMarker !== null) {
            this.map.removeLayer(this.visibleMarker)
        }

        const coordinates: LatLngExpression = [
            parseFloat(selectedSearchResult.lat),
            parseFloat(selectedSearchResult.lon),
        ]

        const toolTipText = this.#createPopUp(selectedSearchResult)

        this.visibleMarker = L.marker(coordinates)
            .bindPopup(toolTipText)
            .openPopup()

        this.map.addLayer(this.visibleMarker)
        this.map.setView(coordinates, 14)
        this.#hideItemList()
    }

    goToResult(selectedSearchResult: SearchResultInterface) {
        this.#addMarker(selectedSearchResult)
        this.alternativeResults.showOpenButton()
        this.alternativeResults.createAlternativeResultsElements(
            selectedSearchResult,
            this.allSearchResults,
            this
        )
    }

    #createPopUp(selectedSearchResult: SearchResultInterface): string {
        const toolTipText = `<p class="text-center">${selectedSearchResult.display_name}<br />(${selectedSearchResult.lat}, ${selectedSearchResult.lon})</p>`

        return toolTipText
    }

    #removeOldResults() {
        const oldResults = Array.from(
            this.itemList.querySelectorAll(
                `div[id^=${globalThis.appShortCut}-result-item-]`
            )
        )
        oldResults.forEach((element) => {
            element.remove()
        })
    }

    #showNoResultHint() {
        this.noResultItem.classList.remove('hidden')
    }

    #hideNoResultHint() {
        this.noResultItem.classList.add('hidden')
    }

    #showItemList() {
        this.itemList.classList.remove('hidden')
    }

    #hideItemList() {
        this.itemList.classList.add('hidden')
    }
}
