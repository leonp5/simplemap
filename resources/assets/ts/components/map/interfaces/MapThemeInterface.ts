export default interface MapThemeInterface {
    urlTemplate: string
    options?: L.TileLayerOptions
}
