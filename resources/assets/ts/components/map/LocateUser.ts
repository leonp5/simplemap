import L, { LatLngExpression, MarkerOptions } from 'leaflet'

export default class LocateUser {
    map: L.Map
    locateButton: HTMLElement
    locateCircle: L.Circle<unknown>
    locatePoint: L.Marker<unknown>

    constructor(map: L.Map) {
        this.map = map
    }

    init(): void {
        this.locateButton = document.getElementById(
            `${globalThis.appShortCut}-locate`
        )

        this.locateButton.addEventListener('click', () => {
            this.locate()
        })
    }

    locate(): void {
        this.addSpinnerToLocateButton()
        navigator.geolocation.getCurrentPosition((position) => {
            const coordinates: LatLngExpression = [
                position.coords.latitude,
                position.coords.longitude,
            ]

            const radius = position.coords.accuracy

            this.drawCircle(coordinates, radius)

            this.drawPoint(coordinates)

            this.map.setView(coordinates, 17)

            this.removeSpinnerFromLocateButton()
        })
    }

    drawCircle(coordinates: LatLngExpression, radius: number): void {
        const circleStyle: L.CircleMarkerOptions = {
            className: 'leaflet-control-locate-circle',
            color: '#136AEC',
            fillColor: '#136AEC',
            fillOpacity: 0.15,
            weight: 0,
        }

        circleStyle.radius = radius

        if (!this.locateCircle) {
            this.locateCircle = L.circle(coordinates, circleStyle).addTo(
                this.map
            )
        } else {
            this.locateCircle
                .setLatLng(coordinates)
                .setRadius(radius)
                .setStyle(circleStyle)
        }
    }

    addSpinnerToLocateButton(): void {
        const icon = this.locateButton.querySelector('i')
        icon.className = 'gg-spinner-two-alt'
        icon.setAttribute('style', '--ggs: 0.7;')

        this.locateButton.classList.remove('p-3')
        this.locateButton.classList.add('p-2')
    }

    removeSpinnerFromLocateButton(): void {
        const icon = this.locateButton.querySelector('i')
        icon.removeAttribute('style')
        icon.className = 'gg-track'
        this.locateButton.classList.remove('p-2')
        this.locateButton.classList.add('p-3')
    }

    drawPoint(coordinates: LatLngExpression): void {
        const pointStyle: L.CircleMarkerOptions = {
            className: 'leaflet-control-locate-marker',
            color: '#fff',
            fillColor: '#2A93EE',
            fillOpacity: 1,
            weight: 3,
            opacity: 1,
            radius: 9,
        }

        const svgIcon = this.createSvgIcon(pointStyle)

        const markerOptions: MarkerOptions = {
            icon: svgIcon,
            draggable: false,
        }

        if (!this.locatePoint) {
            this.locatePoint = L.marker(coordinates, markerOptions).addTo(
                this.map
            )
        } else {
            this.locatePoint.setLatLng(coordinates)
        }
    }

    createSvgIcon(pointOptions: L.CircleMarkerOptions): L.DivIcon {
        const pointStyle = `stroke:${pointOptions.color};stroke-width:${pointOptions.weight};fill:${pointOptions.fillColor};fill-opacity:${pointOptions.fillOpacity};opacity:${pointOptions.opacity};`

        const r = pointOptions.radius
        const w = pointOptions.weight
        const s = r + w
        const s2 = s * 2
        const svg =
            `<svg xmlns="http://www.w3.org/2000/svg" width="${s2}" height="${s2}" version="1.1" viewBox="-${s} -${s} ${s2} ${s2}">` +
            '<circle r="' +
            r +
            '" style="' +
            pointStyle +
            '" />' +
            '</svg>'

        const svgIcon = L.divIcon({
            className: 'leaflet-control-locate-location',
            html: svg,
            iconSize: [s2, s2],
        })

        return svgIcon
    }
}
