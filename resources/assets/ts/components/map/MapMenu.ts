import L, { LeafletMouseEvent } from 'leaflet'

export default class MapMenu {
    map: L.Map
    popup: L.Popup

    constructor(map: L.Map) {
        this.map = map
        this.popup = null
    }

    init() {
        this.map.on('contextmenu', (e: LeafletMouseEvent) => {
            if (this.popup !== null) {
                this.map.closePopup(this.popup)
            }
            this.showMenu(e)
        })
    }

    showMenu(e: LeafletMouseEvent) {
        const options: L.PopupOptions = {
            closeOnClick: true,
        }

        this.popup = L.popup(options)
            .setLatLng(e.latlng)
            .setContent('<pre>Hello</pre>')
            .addTo(this.map)

        this.popup.openOn(this.map)
    }
}
