import L, { LeafletMouseEvent } from 'leaflet'

export default class MapMenu {
    map: L.Map

    constructor(map: L.Map) {
        this.map = map
        this.init()
    }

    init() {
        this.map.on('contextmenu', (e: LeafletMouseEvent) => {
            this.showMenu(e)
        })
    }

    showMenu(e: LeafletMouseEvent) {
        L.popup()
            .setLatLng(e.latlng)
            .setContent('<pre>Hello</pre>')
            .addTo(this.map)
            .openOn(this.map)
    }
}
