import FetchOptionsInterface from './FetchOptionsInterface'

export default class Fetch {
    fetchOptions: FetchOptionsInterface
    token: string

    constructor() {
        this.fetchOptions = { method: '', url: '', token: '' }
        this.token = ''
        this.setToken()
    }

    async doFetch(fetchOptions = this.fetchOptions) {
        const response = await fetch(fetchOptions.url, {
            method: fetchOptions.method,
            credentials: 'same-origin',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json, text-plain, */*',
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': fetchOptions.token
                    ? fetchOptions.token
                    : this.token,
            },
            body: fetchOptions.data,
        })
        return response
    }

    createFetchOptions(form: HTMLFormElement, data: object | null = null) {
        this.fetchOptions = {
            method: form.method,
            url: form.action,
            data: data ? JSON.stringify(data) : null,
            token: this.token,
        }

        return this.fetchOptions
    }

    createFetchOptionsForApi(
        method: string,
        url: string,
        data: object | null = null
    ) {
        this.fetchOptions = {
            method: method,
            url: url,
            data: data ? JSON.stringify(data) : null,
            token: this.token,
        }

        return this.fetchOptions
    }

    setToken(): void {
        if (document.querySelector('meta[name="csrf-token"]')) {
            const csrf = document.querySelector('meta[name="csrf-token"]')

            if (csrf) {
                const content = csrf.getAttribute('content')
                if (content) {
                    this.token = content
                }
            }
        }
    }
}
