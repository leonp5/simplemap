export default interface FetchOptionsInterface {
    method: string
    data?: BodyInit | null | undefined
    url: string
    token: string
}
