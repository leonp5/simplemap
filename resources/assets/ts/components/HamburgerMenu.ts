import InitialClassInterface from '../global/interfaces/InitialClassInterface'

export default class HamburgerMenu implements InitialClassInterface {
    open: boolean
    spanTop: HTMLElement
    spanMiddle: HTMLElement
    spanBottom: HTMLElement
    toggleButton: HTMLElement
    navMenu: Element
    clickOutsideExecute: (e: Event) => void

    constructor() {
        this.open = false
        this.spanTop = null
        this.spanMiddle = null
        this.spanBottom = null
        this.toggleButton = null
        this.navMenu = null
        this.clickOutsideExecute = (e: Event) => {
            this.clickOutside(e, this)
        }
    }

    init(htmlElement: HTMLElement): void {
        this.spanTop = htmlElement.querySelectorAll('span')[0]
        this.spanMiddle = htmlElement.querySelectorAll('span')[1]
        this.spanBottom = htmlElement.querySelectorAll('span')[2]
        this.navMenu =
            htmlElement.getElementsByClassName('sm-hamburger-menu')[0]
        this.toggleButton = htmlElement.querySelector('button')

        this.toggleButton.addEventListener('click', () => {
            this.toggleMenu()
        })
    }

    toggleMenu(): void {
        if (this.open === true) {
            document.removeEventListener(
                'click',
                this.clickOutsideExecute,
                true
            )
        } else {
            document.addEventListener('click', this.clickOutsideExecute, true)
        }
        this.toggleButtonStyle()
        this.toggleMenuStyle()
        this.open = !this.open
    }

    toggleButtonStyle(): void {
        this.spanTop.classList.toggle('-translate-y-1.5')
        this.spanTop.classList.toggle('rotate-45')
        this.spanMiddle.classList.toggle('opacity-0')
        this.spanBottom.classList.toggle('translate-y-1.5')
        this.spanBottom.classList.toggle('-rotate-45')
    }

    toggleMenuStyle(): void {
        this.navMenu.classList.toggle('sm-hamburger-menu-open')
    }

    clickOutside(e: Event, hamburgerMenu: HamburgerMenu): void {
        const clickedElement = e.target as Node
        if (
            !hamburgerMenu.navMenu.contains(clickedElement) &&
            clickedElement !== this.toggleButton
        ) {
            hamburgerMenu.toggleMenu()
        }
    }
}
