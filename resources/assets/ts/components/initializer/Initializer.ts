import * as classes from './classes'

export default class Initializer {
    elementsWithAttributes: Element[]

    constructor() {
        // Must defined before classes are initialized, so they can access this in the constructor
        globalThis.appShortCut = 'sm'

        this.elementsWithAttributes = Array.from(
            document.querySelectorAll(
                `[data-${globalThis.appShortCut}-function]`
            )
        )
    }

    init(): void {
        if (this.elementsWithAttributes.length > 0) {
            this.elementsWithAttributes.forEach((htmlElement) => {
                const className = htmlElement.getAttribute(
                    `data-${globalThis.appShortCut}-function`
                )

                if (className !== null) {
                    const classToExecute = this.factory(className)
                    classToExecute.init(htmlElement)
                }
            })
        }
    }

    factory(className: string) {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        return new (<any>classes)[className]()
    }
}
