import InitialClassInterface from '../../global/interfaces/InitialClassInterface'
import { classes } from './classes'

export default class Initializer {
    elementsWithAttributes: Element[]
    classes: { [key: string]: InitialClassInterface }

    constructor() {
        this.elementsWithAttributes = Array.from(
            document.querySelectorAll(
                `[data-${globalThis.appShortCut}-function]`
            )
        )

        this.classes = classes
    }

    init(): void {
        if (this.elementsWithAttributes.length > 0) {
            this.elementsWithAttributes.forEach((htmlElement) => {
                const className = htmlElement.getAttribute(
                    `data-${globalThis.appShortCut}-function`
                )
                if (className !== null) {
                    const classToExecute = this.classes[className]
                    classToExecute.init(htmlElement)
                }
            })
        }
    }
}
