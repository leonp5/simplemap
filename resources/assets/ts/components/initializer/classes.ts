import InitialClassInterface from '../../global/interfaces/InitialClassInterface'
import HamburgerMenu from '../HamburgerMenu'
import Map from '../map/Map'
import ThemeSwitch from '../theme/ThemeSwitch'

export const classes: { [key: string]: InitialClassInterface } = {
    HamburgerMenu: new HamburgerMenu(),
    Map: new Map(),
    ThemeSwitch: new ThemeSwitch(),
}
