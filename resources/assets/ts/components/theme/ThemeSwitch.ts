import InitialClassInterface from '../../global/interfaces/InitialClassInterface'

export default class ThemeSwitch implements InitialClassInterface {
    themeSettingsMenuWrapper: Element
    currentTheme: string
    toggleThemeSettingsButton: HTMLElement
    themeSettingsMenu: HTMLElement
    themeButtonsRegistered: Boolean

    constructor() {
        this.currentTheme = `${globalThis.appShortCut}-theme-system`
        this.themeButtonsRegistered = false
    }

    init(htmlElement: Element): void {
        this.themeSettingsMenuWrapper = htmlElement
        this.toggleThemeSettingsButton = document.getElementById(
            `${globalThis.appShortCut}-current-theme`
        )
        this.themeSettingsMenu = document.getElementById(
            `${globalThis.appShortCut}-theme-settings`
        )

        this.currentTheme = this.getThemeSettingFromLocalStorage()

        this.setTheme(this.currentTheme)

        this.setIconToToggleButton()

        this.toggleThemeSettingsButton.addEventListener('click', () => {
            this.toggleThemeSettings()
        })
    }

    toggleThemeSettings(): void {
        this.themeSettingsMenu.classList.toggle('hidden')

        if (this.themeButtonsRegistered === false) {
            const themeSetButtons = Array.from(
                this.themeSettingsMenu.querySelectorAll(
                    `div[id^=${globalThis.appShortCut}-theme]`
                )
            )

            themeSetButtons.forEach((themeSetButton) => {
                themeSetButton.addEventListener('click', () => {
                    this.setThemeSetting(themeSetButton)
                })
            })

            this.themeButtonsRegistered = true
        }
    }

    setThemeSetting(themeSetButton: Element): void {
        const selectedTheme = themeSetButton.id
        this.currentTheme = selectedTheme

        this.setIconToToggleButton()

        this.setTheme(selectedTheme)

        if (globalThis.map !== undefined) {
            globalThis.map.invalidateSize()
        }

        this.themeSettingsMenu.classList.add('hidden')
    }

    setTheme(themeName: string): void {
        switch (themeName) {
            case `${globalThis.appShortCut}-theme-dark`:
                this.setDarkTheme()
                this.addThemeSettingToLocalStorage(themeName)
                break
            case `${globalThis.appShortCut}-theme-light`:
                this.setLightTheme()
                this.addThemeSettingToLocalStorage(themeName)
                break

            default:
                this.addThemeSettingToLocalStorage(themeName)
                break
        }
    }

    setIconToToggleButton(): void {
        const themeSetButton = document.getElementById(this.currentTheme)
        const newClassName =
            themeSetButton.querySelector('i[class^=gg]').className
        const className = newClassName + ' cursor-pointer'
        this.toggleThemeSettingsButton.className = className
    }

    setDarkTheme(): void {
        const rootHtml = document.documentElement
        rootHtml.classList.add('dark')
    }

    setLightTheme(): void {
        const rootHtml = document.documentElement
        rootHtml.classList.remove('dark')
    }

    addThemeSettingToLocalStorage(setting: string): void {
        localStorage.setItem(`${globalThis.appShortCut}-theme`, setting)
    }

    getThemeSettingFromLocalStorage(): string {
        const themeSetting = localStorage.getItem(
            `${globalThis.appShortCut}-theme`
        )

        if (themeSetting === null) {
            return `${globalThis.appShortCut}-theme-system`
        }

        return themeSetting
    }
}
