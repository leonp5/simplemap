<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

use Leonp5\SimpleMap\Map\Routes\MapRoutes;
use Leonp5\SimpleMap\Disclaimer\Routes\DisclaimerRoutes;

Route::get('/locale/{locale}', function ($locale) {
    Session::put('locale', $locale);
    return redirect()->back();
})->name('localization');

Route::group([], function () {
    Route::group([], function () {
        (new MapRoutes)->register();
    });
    Route::group([], function () {
        (new DisclaimerRoutes)->register();
    });
});
