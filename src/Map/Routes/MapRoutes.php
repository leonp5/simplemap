<?php

declare(strict_types=1);

namespace Leonp5\SimpleMap\Map\Routes;

use Illuminate\Support\Facades\Route;

use Leonp5\SimpleMap\App\Interfaces\SimpleMapRouteInterface;

class MapRoutes implements SimpleMapRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        Route::get('/', function () {
            return view('map.map');
        })->name('start');
        // Route::get('/', [MapController::class, 'map'])->name('map');
    }
}
