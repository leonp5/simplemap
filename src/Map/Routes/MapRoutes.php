<?php

declare(strict_types=1);

namespace Leonp5\SimpleMap\Map\Routes;

use Illuminate\Support\Facades\Route;

use Leonp5\SimpleMap\Map\Controller\MapController;
use Leonp5\SimpleMap\App\Interfaces\SimpleMapRouteInterface;

class MapRoutes implements SimpleMapRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        Route::get('/', [MapController::class, 'map'])->name('start');
        Route::get('/search/{searchTerms}', [MapController::class, 'withSearchValue'])->name('withSearch');
        Route::get('/xml/download', [MapController::class, 'download'])->name('xmlSearchFileDownload');
    }
}
