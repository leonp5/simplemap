<?php

namespace Leonp5\SimpleMap\Map\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Contracts\Container\BindingResolutionException;

use Leonp5\SimpleMap\App\Controller\Controller;

class MapController extends Controller
{
    /**
     * @return Renderable
     */
    public function map(): Renderable
    {
        return view('map.map');
    }

    /**
     * @param Request $request
     * 
     * @return Renderable 
     */
    public function withSearchValue($searchTerms): Renderable
    {
        return view('map.map', ['searchValue' => $searchTerms]);
    }

    /**
     * @return mixed 
     * @throws BindingResolutionException 
     */
    public function download()
    {
        if (App::environment('local')) {
            $folder = 'dev';
        } else {
            $folder = 'prod';
        }

        $locale = App::getLocale();

        $path = storage_path() . '/app/search/' . $folder . '/' . $locale . '/opensearch.xml';
        $test = file_exists($path);
        if (file_exists($path)) {
            return response()->download($path);
        }
    }
}
