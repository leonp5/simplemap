<?php

declare(strict_types=1);

namespace Leonp5\SimpleMap;

use Illuminate\Support\ServiceProvider;

class SimpleMapProvider extends ServiceProvider
{
    public function register()
    {
        $dependencyProvider = require __DIR__ . '/../config/dependencies.php';

        /* @var $provider SimpleMapDependencyProviderInterface */
        foreach ($dependencyProvider as $provider) {
            $this->app->bind($provider);

            $this->app->make($provider)->provide($this->app);
        }
    }

    public function boot()
    {
    }
}
