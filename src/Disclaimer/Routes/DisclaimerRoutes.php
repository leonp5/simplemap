<?php

declare(strict_types=1);

namespace Leonp5\SimpleMap\Disclaimer\Routes;

use Illuminate\Support\Facades\Route;

use Leonp5\SimpleMap\App\Interfaces\SimpleMapRouteInterface;

class DisclaimerRoutes implements SimpleMapRouteInterface
{
    /**
     * @return void 
     */
    public function register(): void
    {
        Route::get('/disclaimer', function () {
            return view('disclaimer.disclaimer');
        })->name('disclaimer');
    }
}
