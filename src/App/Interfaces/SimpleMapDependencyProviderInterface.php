<?php

declare(strict_types=1);

namespace Leonp5\SimpleMap\App\Interfaces;

use Illuminate\Contracts\Foundation\Application;

interface SimpleMapDependencyProviderInterface
{

    /**
     * @return void
     */
    public function provide(Application $app): void;
}
