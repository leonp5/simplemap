<?php

declare(strict_types=1);

namespace Leonp5\SimpleMap\App\Interfaces;

interface SimpleMapRouteInterface
{

    /**
     * @return void
     */
    public function register(): void;
}
