<?php

namespace Leonp5\SimpleMap\App\Svg;

use Illuminate\View\Component;

class Svg extends Component
{
    /**
     * @param string $svg
     * @param int $width
     * @param int $height
     * @param string $viewBox
     * @param string $fill
     * @param int $strokeWidth
     * @param string $id
     * @param string $class
     * @param string $stroke
     * @param string $rectStroke
     * 
     * @return void 
     */
    public function __construct(
        public $svg = null,
        public $width = 24,
        public $height = 24,
        public $viewBox = '24 24',
        public $fill = 'currentColor',
        public $strokeWidth = 2,
        public $id = null,
        public $class = null,
        public $stroke = null,
    ) {
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.svg');
    }
}
